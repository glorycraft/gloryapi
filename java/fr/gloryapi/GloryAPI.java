package fr.gloryapi;

import fr.gloryapi.utils.Console;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import fr.gloryapi.events.PlayerEvent;

public class GloryAPI extends JavaPlugin{

	private final PluginManager pm = Bukkit.getPluginManager();
	public Console log=new Console();

	@Override
	public void onLoad() {
		log.info(ChatColor.AQUA + "Lancement de GloryAPI");
	}

	@Override
	public void onEnable() {
		pm.registerEvents(new PlayerEvent(this), this);

		log.info("GloryAPI est bien lancé !");


	}
	
	@Override
	public void onDisable() {
		log.info(ChatColor.AQUA + "Désactivation de GloryAPI");
	}
	
}
