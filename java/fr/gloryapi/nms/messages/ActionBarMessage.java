package fr.gloryapi.nms.messages;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;

public class ActionBarMessage {
	
	public static void sendActionBar(Player p, String msg){
		IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \""+msg+"\"}");
		PacketPlayOutChat packetPlayerOutChat = new PacketPlayOutChat(cbc, (byte) 2);
		
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(packetPlayerOutChat);
	}
	
}
