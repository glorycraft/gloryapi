package fr.gloryapi.events;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import fr.gloryapi.GloryAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.massivecraft.factions.entity.MPlayer;

import fr.gloryapi.nms.messages.ActionBarMessage;
import fr.gloryapi.utils.ScoreboardSign;
import fr.gloryapi.utils.Scroller;

public class PlayerEvent implements Listener{

	private GloryAPI plugin;
	private Map<Player, ScoreboardSign> boards = new HashMap<>();
	
	
	public PlayerEvent(GloryAPI main)
	{
		plugin = main;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		if(Bukkit.getServerName().equalsIgnoreCase("Faction")){
			/*
			 * Serveur Faction
			 * 
			 */
			
			for(Entry<Player, ScoreboardSign> sign : boards.entrySet()){
				sign.getValue().setLine(2, "§7 Joueurs: §e"+ Bukkit.getOnlinePlayers().size());
			}
			
			ScoreboardSign sc = new ScoreboardSign(p, "§7GloryCraft § §c"+ Bukkit.getServerName());
			sc.create();
			sc.setLine(0, "§7Faction: "+ MPlayer.get(p).getFactionName());
			sc.setLine(0, "§a");
			sc.setLine(0, "§7§ R§seaux Sociaux: §e/info");
			sc.setLine(1, "§f");
			sc.setLine(2, "§7§ Joueurs: §e"+ Bukkit.getOnlinePlayers().size());
			sc.setLine(3, "§3");
			new BukkitRunnable()
			{
				Scroller scroller = new Scroller("&ebeta.glorycraft.fr", 20, 5, '&');
			 
			    public void run()
			    {
			        sc.setLine(4, scroller.next());
			    }
			}.runTaskTimer(plugin, 0L, 3L); // runs every 3 ticks
			boards.put(p, sc);
			return;
		}
		/*
		 * Serveurs lambas
		 * 
		 */
		for(Entry<Player, ScoreboardSign> sign : boards.entrySet()){
			sign.getValue().setLine(2, "§7§ Joueurs: §e"+ Bukkit.getOnlinePlayers().size());
		}
		
		ScoreboardSign sc = new ScoreboardSign(p, "§7GloryCraft § §c"+ Bukkit.getServerName());
		sc.create();
		sc.setLine(0, "§7§ R§seaux Sociaux: §e/info");
		sc.setLine(1, "§f");
		sc.setLine(2, "§7§ Joueurs: §e"+ Bukkit.getOnlinePlayers().size());
		sc.setLine(3, "§3");


		new BukkitRunnable()
		{
			Scroller scroller = new Scroller("§ebeta.glorycraft.fr", 20, 5, '&');
		 
		    public void run()
		    {
		        sc.setLine(4, scroller.next());     
		    }
		}.runTaskTimer(plugin, 0L, 3L);
		ActionBarMessage.sendActionBar(p, "§bGloryCraft §7§l| §cFaction §7§l| §cMini-Jeux §7§l| §cSkyblock");// runs every 3 ticks
		boards.put(p, sc);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		
		for(Entry<Player, ScoreboardSign> sign : boards.entrySet()){
			sign.getValue().setLine(2, "§7§ Joueurs: §e"+ (Bukkit.getOnlinePlayers().size() - 1));
		}
		
		if(boards.containsKey(p)){
			boards.get(p).destroy();
		}
	}
	
	
	
}
