package fr.gloryapi.utils;

import fr.gloryapi.GloryAPI;
import org.bukkit.Bukkit;

public class APIManagement {
	
	private static GloryAPI main;
	
	public APIManagement(GloryAPI main){
		this.main = main;
	}
	
	public static void reloadPlugin(){
		Bukkit.getPluginManager().disablePlugin(main);
		Bukkit.getPluginManager().enablePlugin(main);
	}
	
	public static void enablePlugin(){
		Bukkit.getPluginManager().enablePlugin(main);
	}
	
	public static void disablePlugin(){
		Bukkit.getPluginManager().disablePlugin(main);
	}
	
}
