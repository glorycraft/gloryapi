package fr.gloryapi.utils;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;

public class Console
{

  private ConsoleCommandSender log = Bukkit.getConsoleSender();


    public void debug(String msg)
    {
        this.log.sendMessage(ChatColor.LIGHT_PURPLE + "[GloryAPI-DEBUG] " + ChatColor.GREEN + ">>>> " + msg);
    }

    public void warning(String msg) {
        log.sendMessage(ChatColor.RED + "[GloryCraft-WARNING] " + ChatColor.YELLOW + " : " + msg);
    }

    public void info(String msg) {
        log.sendMessage(ChatColor.LIGHT_PURPLE + "[GloryAPI-INFO] " + ChatColor.AQUA + " : " + msg);
    }


}

