package fr.gloryapi.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.Main;

	public class SqlConnector {

		private Connection connection;
		private String urlbase,host,db,user,pass;
		public static SqlConnector sqlInstance;
		public static SqlConnector getInstance(){
			return sqlInstance;
		}
		
		public SqlConnector(String urlbase, String host, String db, String user, String pass) {
			this.urlbase = urlbase;
			this.host = host;
			this.db = db;
			this.user = user;
			this.pass = pass;
			
			sqlInstance = this;
		}
		
		public void connect(){
			try {
				connection = DriverManager.getConnection(urlbase + host + "/" + db, user, pass);
				Bukkit.getLogger().log(Level.INFO, "Connect� � la db");
			} catch (SQLException e) {
				Bukkit.getLogger().log(Level.SEVERE, "Erreur de connection � la db");
				e.printStackTrace();
			}
		}
		
		public void disconnect(){
			if(isConnected()){
				try {
					connection.close();
					Bukkit.getLogger().log(Level.INFO, "D�connect� � la db");
				} catch (SQLException e) {
					Bukkit.getLogger().log(Level.SEVERE, "Erreur de d�connection � la db");
					e.printStackTrace();
				}
				return;
			}
			Bukkit.getLogger().log(Level.SEVERE, "Erreur de d�connection � la db");
			
		}
		
		public boolean isConnected(){
			return connection != null;
		}
		
	
}
